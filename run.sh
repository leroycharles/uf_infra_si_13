#!/bin/bash

## @file run.sh
## @brief Main file.
## @details This file contains all functions and scripts calls.
## @author Charles Leroy
## @author Tom Aït-Ouarab

## @fn menu()
## @brief Menu for the tool.
## @details Display the menu and call for the other functions.
## @author Charles Leroy
function menu(){
	clear
	echo "++++MAIN MENU++++"
	echo "1-Install Nextcloud Production"
	echo "2-Install Nextcloud"
	echo "3-Backup Nextcloud"
	echo "0-Exit"
	read uInput
	if [ $uInput -eq 1 ]
	then
		#Install code
		echo "Installing Nextcloud Production"
		sleep 1.5s
		ncProduction
	elif [ $uInput -eq 2 ]
	then
		#Install Code
		echo "Installing Nextcloud"
		sleep 1.5s
		ncConfig
	elif [ $uInput -eq 3 ]
	then
		#Backup Code
		echo "Backing-up Nextcloud"
		sleep 1.5s
		ncBackup
	elif [ $uInput -eq 0 ]
	then
		exit
	else
		echo "Error: wrong input"
		menu
	fi
}

## @fn ncProduction()
## @brief Function that install requirement.
## @details Call a distant script that install requirement for nextcloud.
## @author Tom Aït-Ouarab
function ncProduction(){
	clear
	sudo bash -c "$(wget -q -O - https://raw.githubusercontent.com/nextcloud/vm/master/nextcloud_install_production.sh)"
}


## @fn ncConfig()
## @brief Function that instal nextcloud.
## @details Call a distant script that install nextcloud.
## @author Tom Aït-Ouarab
function ncConfig(){
	clear
	sudo bash -c "$(wget -q -O - https://raw.githubusercontent.com/nextcloud/vm/master/nextcloud-startup-script.sh)"
}


## @fn ncBackup()
## @brief Function that backup current installation of nextcloud.
## @details rsync and data dump.
## @author Tom Aït-Ouarab
function ncBackup(){
	clear
	rsync -Aavx /var/www/nextcloud/ /var/www/nextcloud-dirbkp_`date +"%Y%m%d"`/
	PGPASSWORD="password" pg_dump nextcloud_db -h localhost -U ncAdmin -f /var/www/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
}

echo "Launcher for Nextcloud install"
while :
do
	menu
done
