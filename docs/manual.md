# Manual
## Server requirement ##
### Minimum System Requirements ###

- A clean Ubuntu Server 64-bit 20.04 LTS
- OpenSSH (preferred)
- 20 Gio for OS
- 50 Gio for DATA (/mnt/ncdata)
- Absolute minimum is 1 vCPU and 2 GB RAM (4 GB minimum if you are running any docker containers, 10 GB HDD for OS.)
- A working internet connection (the installation script needs it to download files and variables)

### Recommended ###

- 4 vCPU
- 4096 Mo of RAM
- 40 Gio for OS
- Ports 80 and 443 open to the server.

## Nextcloud Production Install ##
### User Creation ###
When asked, choose to create dedicated user with a temporary password.
### Disk Selection
Select the option "2 disks Auto".

### DNS Provider ###
Choose the local Option.

### Install Apps or software ###
Press OK.

## Nextcloud Startup Script ##
### Additional feature ###
Don't select anything.

### User account ###
When asked change the passwords and create another account.

### Apps ###
WHen asked you should install those apps:
- You can choose either Collabora or OnlyOffice for documents
- LDAP (user management)
- Fail2ban (more security)
- Talk (eq Teams)

It will install Nextcloud Apps directly, you can choose to add or remove apps when the server is 
installed.

## Mail Installation ##
### Mail In A Box Install ###
A mail server can be installed with the following command:

```curl -s https://mailinabox.email/setup.sh | sudo -E bash```

- You will be asked to enter the email address you want and a few other configuration questions. At the end you will be asked for a password for your email address.
- This password will be used to login to webmail, the administrative interface, and on your devices. It will not be used to log onto your Mail-in-a-Box server using SSH (that’s what you did above).
- It is always safe to re-run the setup, either because something went wrong or you just want to see it again. You can do so by following two the steps above again or just running ```sudo mailinabox``` from the command line.

### Mail In A Box Configuration ###
1. Open the URL in Firefox. Firefox will say This Connection is Untrusted.
2. Click on I Understand the Risks.
3. Click on Add Exception.
4. Click on View.
5. Compare the SHA1 Fingerprint in Firefox to the fingerpint reported by Mail-in-a-Box’s setup script in your SSH connection (see the example above). If they match, it is safe to continue.
6. Click on Close. Click on Confirm Security Exception. It is safe (and convenient!) to permanently store the exception.

You can also get a signed SSL certificate later so you don’t have to go through these steps.
