<?php
declare(strict_types=1);

require_once "console.php";

$today = date('Y-m-d-H-i-s');
$user = [];
$content = [];
$exit_bool = 1;
$filename = "project.log";

echo "Daily Meeting 1.0\n";
if (file_exists($filename)) {
    echo "------ LAST ENTRY ------\n";
    displayEntry(lastEntry($filename));
    echo "\n\n";
}

while ($exit_bool == 1) {
    echo "\n1: New Entry\n";
    echo "2: History\n";
    echo "0: Exit\n";
    switch (readline()) {
        case 1:
            if (!newEntry($today, $filename)) {
                $exit_bool = -1;
            }
            break;

        case 2:
            if (!historyEntry($filename)) {
                $exit_bool = -2;
            }
            break;

        case 0:
            echo "Exit";
            $exit_bool = 0;
            break;

        default:
            echo "\nWARNING: INVALID INPUT\n";
            break;
    }
}

return $exit_bool;
