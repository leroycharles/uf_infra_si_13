<?php
declare(strict_types=1);

/**
 * @param array $user
 * @param string $date
 * @param array $content
 * @return string
 * @brief Return a formatted string representing all data from user, date and content.
 */
function flatten(array $user, string $date, array $content): string
{
    return implode("|", [$date, implode("-", $user), implode("^", $content)]);
}

/**
 * @param string $flat
 * @return array
 * @brief Return an array compose of a string date, an array user and an array content.
 */
function deflatten(string $flat): array
{
    $buffer = explode("|", $flat);
    return ["date" => $buffer[0], "user" => explode("-", $buffer[1]), "content" => explode("^", $buffer[2])];
}

/**
 * @param string $filename
 * @return string
 * @brief Return last entry in file, project file is default.
 */
function lastEntry(string $filename = "project.log"): string
{
    $stream = fopen($filename, "r");
    $buffer = "";
    while (!feof($stream)) {
        $buffer = fgets($stream);
    }
    fclose($stream);
    return $buffer;
}

/**
 * @param string $entry
 * @param string $filename
 * @return bool
 * @brief Append a string to the file and return if the operation is a success.
 */
function appendEntry(string $entry, string $filename = "project.log"): bool
{
    $stream = fopen($filename, "a");
    $out = fputs($stream, $entry) != false;
    fclose($stream);
    return $out;
}

/**
 * @param string $flat
 * @return bool
 * @brief Display Header and Content of a flatten entry, return false if string can not be deflatten.
 */
function displayEntry(string $flat): bool
{
    $buffer = deflatten($flat);
    if ($buffer != is_array($buffer) OR $buffer == []) {
        return false;
    } else {
        echo "--- Header ---\n";
        echo sprintf("Date: %s\n", $buffer['date']);
        echo sprintf("User: %s<%s>\n", $buffer['user'][0], $buffer['user'][1]);
        echo "--- Content ---\n";
        echo sprintf("Yesterday Summary: %s\n", $buffer['content'][0]);
        echo sprintf("Today Summary: %s\n", $buffer['content'][1]);
        echo sprintf("Issues Summary: %s\n", $buffer['content'][2]);
        return true;
    }
}

/**
 * @param string $date
 * @param string $filename
 * @return bool
 * @brief Read user input and appends it to file.
 */
function newEntry(string $date, string $filename = "project.log"): bool
{
    $user = [];
    $content = [];
    echo "------ NEW ENTRY ------\n";
    echo "--- Header ---\n";
    echo sprintf("Date: %s\n", $date);
    echo "User Name: ";
    $user[0] = readline();
    echo "User Email: ";
    $user[1] = readline();
    echo "--- Content ---\n";
    echo "Yesterday Summary: ";
    $content[0] = readline();
    echo "Today Summary: ";
    $content[1] = readline();
    echo "Issues Summary: ";
    $content[2] = readline();

    return appendEntry(flatten($user, $date, $content), $filename);
}

/**
 * @param string $filename
 * @return bool
 * @brief Display history, return false if file exists but error on display or string recuperation.
 */
function historyEntry(string $filename = "project.log"): bool
{
    if (!file_exists($filename)) {
        echo "NO LOG NO ENTRY TO SHOW";
        return true;
    } else {
        $stream = fopen($filename, "r");
        while (!feof($stream)) {
            if (!displayEntry(fgets($stream))) {
                fclose($stream);
                return false;
            }
        }
        fclose($stream);
        return true;
    }
}